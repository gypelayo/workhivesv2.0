package models

import (
	"time"
)

// Group represents the group entity for the chat application
type Group struct {
	ID       int           `gorm:"primaryKey"` // Automatically uses auto increment in PostgreSQL
	Name     string        `gorm:"size:255;not null"`
	OwnerID  int           `gorm:"not null"`           // Foreign key that references the Users table
	Owner    User          `gorm:"foreignKey:OwnerID"` // Specifies Owner as a User type with OwnerID as FK
	Members  []GroupMember `gorm:"foreignKey:GroupID"` // Has many relationship with GroupMembers
	Messages []Message     `gorm:"foreignKey:GroupID"` // Has many relationship with Messages
}

// GroupInput is used for creating a new group, typically from JSON input
type GroupInput struct {
	OwnerUsername   string   `json:"owner_username" binding:"required"`
	GroupName       string   `json:"group_name" binding:"required"`
	MemberUsernames []string `json:"member_usernames"` // No binding:"required" to allow groups with just an owner
}

// GroupMember represents the relationship between a group and its members
type GroupMember struct {
	GroupID  int       `gorm:"primaryKey"`
	UserID   int       `gorm:"primaryKey"`
	JoinedAt time.Time `gorm:"default:now()"`
	User     User      `gorm:"foreignKey:UserID"` // Specifies User as a User type with UserID as FK
}
