package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"chat-server/internal/models"
	"chat-server/internal/services"
)

func PostMessageToGroup(messageService services.MessageService) gin.HandlerFunc {
	return func(c *gin.Context) {
		var messageInput models.MessageInput

		// Parse the JSON body into the messageInput struct
		if err := c.ShouldBindJSON(&messageInput); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		// Call the service to post the message
		err := messageService.PostMessage(&messageInput)
		if err != nil {
			// Respond with an error status and message if something goes wrong
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		// If successful, return a success message
		c.JSON(http.StatusOK, gin.H{"message": "Message posted successfully!"})
	}
}
