package repositories

import (
	"chat-server/internal/models"

	"github.com/jinzhu/gorm"
)

type UserRepository interface {
	GetUser(id int) (models.User, error)
	RegisterUser(user *models.User) error
	GetUserByEmail(email string) *models.User
	GetUserByUsername(username string) *models.User
}

type userRepo struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepository {
	return &userRepo{db}
}

func (r *userRepo) GetUser(id int) (models.User, error) {
	var user models.User
	if err := r.db.First(&user, id).Error; err != nil {
		return user, err
	}

	return user, nil
}

func (r *userRepo) RegisterUser(user *models.User) error {
	return r.db.Create(user).Error
}

func (r *userRepo) GetUserByEmail(email string) *models.User {
	var user models.User

	if err := r.db.Where("email = ?", email).First(&user).Error; err != nil {
		// Return nil if no user found
		return nil
	}

	return &user
}

func (r *userRepo) GetUserByUsername(username string) *models.User {
	var user models.User

	if err := r.db.Where("name = ?", username).First(&user).Error; err != nil {
		// Return nil if no user found
		return nil
	}

	return &user
}
