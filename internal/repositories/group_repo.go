package repositories

import (
	"chat-server/internal/models"
	"time"

	"github.com/jinzhu/gorm"
)

type GroupRepository interface {
	GetGroup(id int) (models.Group, error)
	RegisterGroup(user *models.GroupInput) error
}

type groupRepo struct {
	db *gorm.DB
}

func NewGroupRepository(db *gorm.DB) GroupRepository {
	return &groupRepo{db}
}

func (r *groupRepo) GetGroup(id int) (models.Group, error) {
	var group models.Group
	if err := r.db.First(&group, id).Error; err != nil {
		return group, err
	}

	return group, nil
}

// func (r *groupRepo) RegisterGroup(group *models.Group) error {
// 	return r.db.Create(group).Error
// }

func (s *groupRepo) RegisterGroup(input *models.GroupInput) error {
	// Resolve OwnerID from OwnerUsername
	var owner models.User
	if err := s.db.Where("name = ?", input.OwnerUsername).First(&owner).Error; err != nil {
		return err // Owner not found
	}

	// Create Group
	group := models.Group{
		Name:    input.GroupName,
		OwnerID: owner.ID,
	}
	if err := s.db.Create(&group).Error; err != nil {
		return err // Error creating group
	}

	// Resolve UserIDs for each MemberUsername and add to GroupMembers
	for _, username := range input.MemberUsernames {
		var member models.User
		if err := s.db.Where("name = ?", username).First(&member).Error; err != nil {
			return err // Member not found
		}

		groupMember := models.GroupMember{
			GroupID:  group.ID,
			UserID:   member.ID,
			JoinedAt: time.Now(), // Assuming you want to set the join time at creation
		}
		if err := s.db.Create(&groupMember).Error; err != nil {
			return err // Error creating group member
		}
	}

	return nil
}
