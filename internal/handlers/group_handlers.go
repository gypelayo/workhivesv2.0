package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"chat-server/internal/models"
	"chat-server/internal/services"
)

func RegisterGroup(groupService services.GroupService) gin.HandlerFunc {
	return func(c *gin.Context) {

		groupInput := models.GroupInput{}

		if err := c.ShouldBindJSON(&groupInput); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		// Create user
		err := groupService.RegisterGroup(&groupInput)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		// Return created user
		c.JSON(http.StatusOK, gin.H{"message": "Group created successfully!"})
	}
}
