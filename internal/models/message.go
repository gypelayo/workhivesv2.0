package models

import "time"

type Message struct {
	ID       int `gorm:"primaryKey"`
	Content  string
	SenderID int       `gorm:"not null"`
	GroupID  int       `gorm:"not null"`
	SentAt   time.Time `gorm:"default:now()"`
	Sender   User      `gorm:"foreignKey:SenderID"` // Specifies Sender as a User type with SenderID as FK
}

type MessageInput struct {
	Username  string `json:"username" binding:"required"`
	GroupName string `json:"group_name" binding:"required"`
	Content   string `json:"content" binding:"required"`
}
