package services

import (
	"chat-server/internal/models"
	"chat-server/internal/repositories"
)

type UserService struct {
	userRepo repositories.UserRepository
}

func NewUserService(repo repositories.UserRepository) UserService {
	return UserService{repo}
}

func (s *UserService) RegisterUser(user *models.User) error {
	return s.userRepo.RegisterUser(user)
}

func (s *UserService) EmailExists(email string) bool {
	return s.userRepo.GetUserByEmail(email) != nil
}
func (s *UserService) UsernameExists(username string) bool {
	return s.userRepo.GetUserByUsername(username) != nil
}
