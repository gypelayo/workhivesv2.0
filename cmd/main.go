package main

import (
	"chat-server/internal/database"
	"chat-server/internal/handlers"
	"chat-server/internal/repositories"
	"chat-server/internal/services"

	"github.com/gin-gonic/gin"
)

func main() {

	db := database.InitDB()

	userRepo := repositories.NewUserRepository(db)
	groupRepo := repositories.NewGroupRepository(db)
	messageRepo := repositories.NewMessageRepository(db)
	// messageRepo := repositories.NewMessageRepository(db)

	userService := services.NewUserService(userRepo)
	groupService := services.NewGroupService(groupRepo)
	messageService := services.NewMessageService(messageRepo)
	// messageService := services.NewMessageService(messageRepo)

	router := gin.Default()

	router.POST("/users/create", handlers.RegisterUser(userService))
	router.POST("/groups/create", handlers.RegisterGroup(groupService))
	router.POST("/messages/create", handlers.PostMessageToGroup(messageService))
	// router.POST("/messages", messageHandler(&messageService))

	router.Run(":8080")
}
