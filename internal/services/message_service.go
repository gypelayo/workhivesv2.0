package services

import (
	"chat-server/internal/models"
	"chat-server/internal/repositories"
)

type MessageService struct {
	messageRepo repositories.MessageRepository
}

func NewMessageService(repo repositories.MessageRepository) MessageService {
	return MessageService{repo}
}

func (s *MessageService) PostMessage(message *models.MessageInput) error {
	return s.messageRepo.PostMessage(message)
}
