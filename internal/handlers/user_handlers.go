package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"chat-server/internal/models"
	"chat-server/internal/services"
)

func RegisterUser(userService services.UserService) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get user input
		userInput := models.User{}

		if err := c.ShouldBindJSON(&userInput); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		if userService.EmailExists(userInput.Email) {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Email already exists"})
			return
		}
		if userService.UsernameExists(userInput.Name) {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Name already exists"})
			return
		}
		// Create user
		err := userService.RegisterUser(&userInput)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		// Return created user
		c.JSON(http.StatusOK, gin.H{"message": "User created successfully!"})
	}
}
