package repositories

import (
	"chat-server/internal/models"
	"errors"
	"time"

	"github.com/jinzhu/gorm"
)

type MessageRepository interface {
	PostMessage(message *models.MessageInput) error
}

type messageRepo struct {
	db *gorm.DB
}

func NewMessageRepository(db *gorm.DB) MessageRepository {
	return &messageRepo{db}
}

func (r *messageRepo) PostMessage(input *models.MessageInput) error {
	var user models.User
	var group models.Group

	// Fetch the user by username
	if err := r.db.Where("name = ?", input.Username).First(&user).Error; err != nil {
		return errors.New("user not found")
	}

	// Fetch the group by group name
	if err := r.db.Where("name = ?", input.GroupName).First(&group).Error; err != nil {
		return errors.New("group not found")
	}

	// Check if the user is a member of the group
	var count int
	r.db.Model(&models.GroupMember{}).Where("group_id = ? AND user_id = ?", group.ID, user.ID).Count(&count)
	if count == 0 {
		return errors.New("user is not a member of the group")
	}

	// At this point, we know the user exists, the group exists, and the user is a member of the group.
	// Now, create and save the message.
	message := models.Message{
		Content:  input.Content,
		SenderID: user.ID,
		GroupID:  group.ID,
		SentAt:   time.Now(),
	}

	return r.db.Create(&message).Error
}
