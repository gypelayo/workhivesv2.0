package services

import (
	"chat-server/internal/models"
	"chat-server/internal/repositories"
)

type GroupService struct {
	groupRepo repositories.GroupRepository
}

func NewGroupService(repo repositories.GroupRepository) GroupService {
	return GroupService{repo}
}

func (s *GroupService) RegisterGroup(group *models.GroupInput) error {
	return s.groupRepo.RegisterGroup(group)
}
