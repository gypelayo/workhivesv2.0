package database

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func InitDB() *gorm.DB {
	db, err := gorm.Open("postgres", "host=localhost port=5432 user=guilherme dbname=chatapp sslmode=disable password=password")
	if err != nil {
		panic(err)
	}

	return db
}
